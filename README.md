# n_ToF build env

Docker images for n_ToF project

## Install

To use the docker build image:
```bash
docker pull gitlab-registry.cern.ch/ntof/daq/builder

# Will bind current directory to /root in docker container
docker run --rm -it -v $PWD:/root -w /root gitlab-registry.cern.ch/ntof/daq/builder bash
```

## docker-builder Script

A convenience script is provided to start the proper Docker environment for a given project.

This script will look for a .docker-builder file in current directory and spawn a docker container mounting proper directories.

To use this script:
```bash
# From a project directory
docker-builder bash
```
